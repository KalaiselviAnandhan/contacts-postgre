const pool = require("./connection")


const contactCreation = `
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

CREATE TABLE IF NOT EXISTS contacts(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(), 
    name TEXT NOT NULL, 
    ph BIGINT NOT NULL,
    email VARCHAR(200) NOT NULL,
    address TEXT NOT NULL,
    img TEXT NOT NULL);`

const contactValues = `INSERT INTO contacts(name,ph,email,address,img) VALUES
    ('Anandhan',1234567892,'xyz@gmail.com','Bangalore','person.jpg'),
    ('Naga Anand',1234567891,'naagaanand@gmail.com','Bangalore','person.jpg'),
    ('Subram',1234567893,'subram@gmail.com','Bangalore','person.jpg');`

function contactsTableCreation(){
    return new Promise((resolve, reject)=>{
        pool.query(contactCreation, function(error,result){
            if(error){
                reject(error)
            }

            pool.query(`SELECT COUNT(*) AS counter FROM contacts`,(error,result)=>{
                if(error){ 
                    reject(error)
                }

                if(result.rows[0].counter === "0"){
                    pool.query(contactValues, (error)=>{
                        if(error){
                            reject(error)
                        }
                        resolve()
                    })
                }
            })
        })
    })
}

exports.contactsTableCreation = contactsTableCreation
