const {Pool} = require('pg')
const config = require('../config')

const configData = {
    user : config.USERNAME,
    host : config.HOST,
    password : config.PASSWORD,
    database : config.DATABASE_NAME
}

const pool = new Pool(configData)

module.exports = pool